﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DiagnosticAdapter.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ChargeToGo.API.Models;
using System.Xml;
using System.Drawing;
using GeoCoordinatePortable;
using ChargeToGo.API.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace ChargeToGo.API.Controllers
{
   // [Authorize]
    [Route("api/chargepoint")]
    public class ChargePointController : Controller
    {
        string baseURL = "https://api.openchargemap.io/v2/poi/?output=xml&distanceUnit=KM&latitude=";
        ChargePointHelpers helper = new ChargePointHelpers();

        public async Task<string> GetRequest(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                {
                    using (HttpContent content = response.Content)
                    {
                        return content.ReadAsStringAsync().Result;
                    }
                }
            }
        }

        [HttpGet("{latitude}/{longitude}/{distance}")]
        public IActionResult GetPointsRange(double latitude, double longitude,int distance)
        {
            string url = baseURL + latitude+"&longitude="+longitude+"&distance="+ distance;
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var longitudeNodes = xmlResult.GetElementsByTagName("Longitude");
            var latitudeNodes = xmlResult.GetElementsByTagName("Latitude");
            var titleNodes = xmlResult.GetElementsByTagName("LocationTitle");
            var infoNodes = xmlResult.GetElementsByTagName("ConnectionInfo");

            List<ChargePointDto> chargepoints = new List<ChargePointDto>();

            for (int i = 0; i < longitudeNodes.Count; i++)
            {
                var title = titleNodes[i].InnerText;
                var latitudePoint = latitudeNodes[i].InnerText;
                var longitudePoint = longitudeNodes[i].InnerText;
                var lastInfo = infoNodes[i].Attributes["LevelName"].Value+ ", Amps : "+infoNodes[i].Attributes["Amps"].Value+", Voltage : "+infoNodes[i].Attributes["Voltage"].Value + ", Status " + infoNodes[i].Attributes["Status"].Value;

                ChargePointDto chargepoint = new ChargePointDto(title, latitudePoint, longitudePoint, lastInfo);

                chargepoints.Add(chargepoint);
            }

            return Ok(chargepoints);
        }

        //[HttpGet("angle/{latitudeStart}/{longitudeStart}/{latitudeDest}/{longitudeDest}")]
        public List<ChargePointDto> GetAnglePoints(double latitudeStart, double longitudeStart, double latitudeDest, double longitudeDest)
        {
            Coordinate _center = new Coordinate(longitudeStart, latitudeStart);
            Coordinate _dest = new Coordinate(longitudeDest, latitudeDest);

            double radiuscircle = _center.GetDistance(_dest);

            double angle = Math.Atan2(_dest.Y - _center.Y, _dest.X - _center.X);
            double angledegree = helper.RadianToDegree(angle);
            double anglestart = helper.DegreeToRadian(angledegree - 25);
            double angleend = helper.DegreeToRadian(angledegree + 25);
            double radiuskm = helper.GetDistanceBetweenPoints(latitudeStart, latitudeDest, longitudeStart, longitudeDest)+10;

            string url = baseURL + latitudeStart + "&longitude=" + longitudeStart + "&distance=" + radiuskm;
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var longitudeNodes = xmlResult.GetElementsByTagName("Longitude");
            var latitudeNodes = xmlResult.GetElementsByTagName("Latitude");
            var titleNodes = xmlResult.GetElementsByTagName("LocationTitle");
            var infoNodes = xmlResult.GetElementsByTagName("ConnectionInfo");

            List<ChargePointDto> chargepoints = new List<ChargePointDto>();

            for (int i = 0; i < longitudeNodes.Count; i++)
            {
                Coordinate temp = new Coordinate(Convert.ToDouble(longitudeNodes[i].InnerText), Convert.ToDouble(latitudeNodes[i].InnerText));
                double anglepoint = Math.Atan2(temp.Y - _center.Y, temp.X - _center.X);
                double radiuspoint = _center.GetDistance(temp);
                if (temp.IsInsideSector(anglepoint,radiuspoint,anglestart,angleend,radiuscircle))
                {
                    var title = titleNodes[i].InnerText;
                    var latitudePoint = latitudeNodes[i].InnerText;
                    var longitudePoint = longitudeNodes[i].InnerText;


                    var levelname = helper.CheckInfo(infoNodes[i], "LevelName");
                    var amps = helper.CheckInfo(infoNodes[i], "Amps");
                    var voltage = helper.CheckInfo(infoNodes[i], "Voltage");
                    var status = helper.CheckInfo(infoNodes[i], "Status");
                    var lastInfo = levelname + ", Amps : " + amps + ", Voltage : " + voltage + ", Status : " + status;

                    ChargePointDto chargepoint = new ChargePointDto(title, latitudePoint, longitudePoint, lastInfo);

                    chargepoints.Add(chargepoint);
                }
            }

            return chargepoints;
        }

        [HttpGet("destination/{location}")]
        public IActionResult GetDestination(string location)
        {
            string url = "https://maps.google.com/maps/api/geocode/xml?address="+location+"&key=AIzaSyBeT4UxwuGgyndiaiagBgY-thD09SvOEGE";
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var destination = xmlResult.GetElementsByTagName("location");
            
            return Ok(destination[0].OuterXml);
        }
        
        public List<Step> GetSteps(double latitudeStart, double longitudeStart, double latitudeDest, double longitudeDest)
        {
            string url = "https://maps.googleapis.com/maps/api/directions/xml?origin=" + latitudeStart + "," + longitudeStart + "&destination=" + latitudeDest + "," + longitudeDest + "&key=AIzaSyBeT4UxwuGgyndiaiagBgY-thD09SvOEGE";
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var route = xmlResult.GetElementsByTagName("step");
            List<Step> steps = new List<Step>();

            for (int i = 0; i < route.Count; i++)
            {
                var nodes = route[i].ChildNodes;

                Coordinate start = new Coordinate(Convert.ToDouble(nodes[1].SelectSingleNode("lng").InnerText), Convert.ToDouble(nodes[1].SelectSingleNode("lat").InnerText));
                Coordinate end = new Coordinate(Convert.ToDouble(nodes[2].SelectSingleNode("lng").InnerText), Convert.ToDouble(nodes[2].SelectSingleNode("lat").InnerText));

                string distancenode = nodes[6].SelectSingleNode("text").InnerText;
                string[] distance = distancenode.Split(null);
                double distancefinal = Convert.ToDouble(distance[0]);
                string units = distance[1];

                Step temp = new Step(start, end,distancefinal,units);
                steps.Add(temp);
            }

            return steps;
        }

        [HttpGet("route/{latitudeStart}/{longitudeStart}/{latitudeDest}/{longitudeDest}/{eautonomy}")]
        public IActionResult GetRoute(double latitudeStart, double longitudeStart, double latitudeDest, double longitudeDest, double eautonomy)
        {
            string url = "https://maps.googleapis.com/maps/api/directions/xml?origin=" + latitudeStart + "," + longitudeStart + "&destination=" + latitudeDest + "," + longitudeDest + "&key=AIzaSyBeT4UxwuGgyndiaiagBgY-thD09SvOEGE";
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            double autonomy = eautonomy;

            var distancenode = xmlResult.GetElementsByTagName("distance");
            string totaldistance = distancenode[distancenode.Count - 1].SelectSingleNode("text").InnerText;
            string[] distance = totaldistance.Split(null);
            double distancefinal = Convert.ToDouble(distance[0]);
            string units = distance[1];

            List<Step> steps = GetSteps(latitudeStart, longitudeStart, latitudeDest, longitudeDest);
            List<ChargePointDto> chargepoints = new List<ChargePointDto>();

            if (autonomy > distancefinal && units.Equals("km"))
            {
                List<ChargePointDto> cptemp = new List<ChargePointDto>();
                List<ChargePointDto> cptemp2 = new List<ChargePointDto>();
                cptemp = GetAnglePoints(latitudeStart, longitudeStart, latitudeDest, longitudeDest);
                cptemp2 = GetAnglePoints(latitudeDest, longitudeDest, latitudeStart, longitudeStart);
                helper.AddListToList(cptemp, chargepoints);
                helper.AddListToList(cptemp2, chargepoints);
            }
            if (autonomy < distancefinal && units.Equals("km"))
            {
                int i = 0;
                while (i != steps.Count)
                {
                    int initial = i;
                    double tempdistance = 0;
                    do
                    {
                        if (steps[i].Units.Equals("m"))
                        {
                            tempdistance = tempdistance + (steps[i].Distance)*10E-3;
                        }
                        else
                        {
                            tempdistance = tempdistance + steps[i].Distance;
                        }
                        i++;
                    } while (tempdistance < autonomy && i != steps.Count);

                    int final = 0;
                    if(i == steps.Count)
                    {
                        final = i-1;
                    }
                    else
                    {
                        final = i;
                    }

                    List<ChargePointDto> cptemp = new List<ChargePointDto>();
                    List<ChargePointDto> cptemp2 = new List<ChargePointDto>();
                    cptemp = GetAnglePoints(steps[initial].StartLocation.Y, steps[initial].StartLocation.X, steps[final].EndLocation.Y, steps[final].EndLocation.X);
                    cptemp2 = GetAnglePoints(steps[final].EndLocation.Y, steps[final].EndLocation.X, steps[initial].StartLocation.Y, steps[initial].StartLocation.X);
                    helper.AddListToList(cptemp, chargepoints);
                    helper.AddListToList(cptemp2, chargepoints);
                }
                
            }

            return Ok(chargepoints);
        }

        public List<ChargePointDto> GetNearby(double latitude, double longitude, double distance)
        {
            string url = baseURL + latitude + "&longitude=" + longitude + "&distance=" + distance;
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var longitudeNodes = xmlResult.GetElementsByTagName("Longitude");
            var latitudeNodes = xmlResult.GetElementsByTagName("Latitude");
            var titleNodes = xmlResult.GetElementsByTagName("LocationTitle");
            var infoNodes = xmlResult.GetElementsByTagName("ConnectionInfo");

            List<ChargePointDto> chargepoints = new List<ChargePointDto>();

            for (int i = 0; i < longitudeNodes.Count; i++)
            {
                var title = titleNodes[i].InnerText;
                var latitudePoint = latitudeNodes[i].InnerText;
                var longitudePoint = longitudeNodes[i].InnerText;

                var levelname = helper.CheckInfo(infoNodes[i], "LevelName");
                var amps = helper.CheckInfo(infoNodes[i], "Amps");
                var voltage = helper.CheckInfo(infoNodes[i], "Voltage");
                var status = helper.CheckInfo(infoNodes[i], "Status");
                var lastInfo = levelname + ", Amps : " + amps + ", Voltage : " + voltage + ", Status : " + status;

                ChargePointDto chargepoint = new ChargePointDto(title, latitudePoint, longitudePoint, lastInfo);

                chargepoints.Add(chargepoint);
            }

            return chargepoints;
        }
       
        [HttpGet("angle/{latitudeStart}/{longitudeStart}/{latitudeDest}/{longitudeDest}")]
        public IActionResult GetAngleMarkers(double latitudeStart, double longitudeStart, double latitudeDest, double longitudeDest)
        {
            Coordinate _center = new Coordinate(longitudeStart, latitudeStart);
            Coordinate _dest = new Coordinate(longitudeDest, latitudeDest);

            double radiuscircle = _center.GetDistance(_dest);

            double angle = Math.Atan2(_dest.Y - _center.Y, _dest.X - _center.X);
            double angledegree = helper.RadianToDegree(angle);
            double anglestart = helper.DegreeToRadian(angledegree - 25);
            double angleend = helper.DegreeToRadian(angledegree + 25);
            double radiuskm = helper.GetDistanceBetweenPoints(latitudeStart, latitudeDest, longitudeStart, longitudeDest) + 10;

            string url = baseURL + latitudeStart + "&longitude=" + longitudeStart + "&distance=" + radiuskm;
            var content = GetRequest(url);

            var xmlResult = new XmlDocument();
            xmlResult.LoadXml(content.Result);

            var longitudeNodes = xmlResult.GetElementsByTagName("Longitude");
            var latitudeNodes = xmlResult.GetElementsByTagName("Latitude");
            var titleNodes = xmlResult.GetElementsByTagName("LocationTitle");
            var infoNodes = xmlResult.GetElementsByTagName("ConnectionInfo");

            List<ChargePointDto> chargepoints = new List<ChargePointDto>();

            for (int i = 0; i < longitudeNodes.Count; i++)
            {
                Coordinate temp = new Coordinate(Convert.ToDouble(longitudeNodes[i].InnerText), Convert.ToDouble(latitudeNodes[i].InnerText));
                double anglepoint = Math.Atan2(temp.Y - _center.Y, temp.X - _center.X);
                double radiuspoint = _center.GetDistance(temp);
                if (temp.IsInsideSector(anglepoint, radiuspoint, anglestart, angleend, radiuscircle))
                {
                    var title = titleNodes[i].InnerText;
                    var latitudePoint = latitudeNodes[i].InnerText;
                    var longitudePoint = longitudeNodes[i].InnerText;
                    
                    var levelname = helper.CheckInfo(infoNodes[i], "LevelName");
                    var amps = helper.CheckInfo(infoNodes[i], "Amps");
                    var voltage = helper.CheckInfo(infoNodes[i], "Voltage");
                    var status = helper.CheckInfo(infoNodes[i], "Status");

                    var lastInfo = levelname + ", Amps : " + amps + ", Voltage : " + voltage + ", Status : " + status;

                    ChargePointDto chargepoint = new ChargePointDto(title, latitudePoint, longitudePoint, lastInfo);

                    chargepoints.Add(chargepoint);
                }
            }

            return Ok(chargepoints);
        }

    }
} 

    
    
        

