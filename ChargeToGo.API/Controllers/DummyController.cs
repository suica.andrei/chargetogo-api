﻿using ChargeToGo.API.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Controllers
{
    public class DummyController : Controller
    {
        private PointInfoContext _ctx;

        public DummyController (PointInfoContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        [Route("api/tstdb")]
        public IActionResult TstDb()
        {
            return Ok();
        }
    }
}
