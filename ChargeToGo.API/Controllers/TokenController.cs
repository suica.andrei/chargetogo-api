﻿using ChargeToGo.API.Entities;
using ChargeToGo.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ChargeToGo.API.Controllers
{
    [Route("api/token")]
    public class TokenController : Controller
    {
        private PointInfoContext _ctx;
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration, PointInfoContext ctx)
        {
            _configuration = configuration;
            _ctx = ctx;

        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody]UserDto loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var userId = GetUserIdFromCredentials(loginViewModel);
                if (userId == -1)
                {
                    return Unauthorized();
                }

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,loginViewModel.Username),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };

                var token = new JwtSecurityToken(
                     issuer: "aplicatiemobil",
                     audience: "apicharge",
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(60),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("cb8a670a - 7481 - 4dd7 - 9ef7 - 37fd5a0ee505")),
                         SecurityAlgorithms.HmacSha256)
                );

                return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }

            return BadRequest();
        }

        private int GetUserIdFromCredentials(UserDto loginViewModel)
        {
            //var userId = -1;
            //if(loginViewModel.Username == "amuraj" && loginViewModel.Password == "SuperSecret")
            //{
            //    userId = 5;
            //}
            //return userId;

            var user = _ctx.Users.Where(x => x.Username == loginViewModel.Username && x.Password == loginViewModel.Password).ToList().FirstOrDefault();

            if (user == null)
                return -1;

            return user.Id;
        }
    }
}
