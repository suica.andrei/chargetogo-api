﻿using ChargeToGo.API.Entities;
using ChargeToGo.API.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private PointInfoContext _ctx;

        public UserController(PointInfoContext ctx)
        {
            _ctx = ctx;
        }

        [HttpPost("create")]
        public IActionResult CreateUser([FromBody] User user)
        {
            if (user == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var finaluser = new User()
            {
                Username = user.Username,
                Password = user.Password
            };

            _ctx.Users.Add(finaluser);
            _ctx.SaveChanges();

            return Ok("User created");
        }
    }
}
