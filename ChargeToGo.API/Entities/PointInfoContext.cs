﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Entities
{
    public class PointInfoContext : DbContext
    {
        public PointInfoContext()
        {
        }

        public PointInfoContext(DbContextOptions<PointInfoContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<ChargePoint> ChargePoints { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
