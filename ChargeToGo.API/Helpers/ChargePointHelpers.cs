﻿using ChargeToGo.API.Models;
using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace ChargeToGo.API.Helpers
{
    public class ChargePointHelpers
    {
        public double RadianToDegree(double angletodegree)
        {
            return angletodegree * (180.0 / Math.PI);
        }

        public double DegreeToRadian(double angletoradian)
        {
            return Math.PI * angletoradian / 180.0;
        }

        public string CheckInfo(XmlNode info,string attribute)
        {
            if(info.Attributes[attribute] == null)
            {
                return "No info";
            }
            else
            {
                return info.Attributes[attribute].Value; 
            }
        }

        public double GetDistanceBetweenPoints(double lat1, double lat2, double lon1, double lon2)
        {
            var sCoord = new GeoCoordinate(lat1, lon1);
            var eCoord = new GeoCoordinate(lat2, lon2);

            return sCoord.GetDistanceTo(eCoord) / 1000;
        }

        public void AddListToList(List<ChargePointDto> toAdd , List<ChargePointDto> added)
        {
            for(int i = 0; i < toAdd.Count; i++)
            {
                added.Add(toAdd[i]);
            }
        }
    }
}
