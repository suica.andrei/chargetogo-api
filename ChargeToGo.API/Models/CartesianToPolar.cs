﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class CartesianToPolar
    {
        public double Radius { get; set; }

        public double AngleRadian { get; set; }

        public double AngleDegree { get; set; }

        public CartesianToPolar(double X,double Y)
        {
            this.Radius = Math.Pow((Math.Pow(X, 2) + Math.Pow(Y, 2)), 0.5);
            this.AngleRadian = Math.Atan2(Y, X);
        }

        public double GetAngleDegree()
        {
            return AngleRadian * 360 / (2 * Math.PI);
        }
    }
}
