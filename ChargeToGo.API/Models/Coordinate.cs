﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class Coordinate
    {
        public double X { get; set; }

        public double Y { get; set; }

        public Coordinate(double x, double y)
        {
            this.X = x;

            this.Y = y;
        }

        public double GetDistance(Coordinate destination)
        {
            double radius = Math.Pow((destination.X - this.X), 2) + Math.Pow((destination.Y - this.Y), 2);
            return Math.Sqrt(radius);
        }

        public bool AreClockwise(double angle1, double angle2)
        {
            return angle1 >= angle2;
        }
        public bool IsWithinRadius(double p, double radius)
        {
            return radius >= p;
        }

        public bool IsInsideSector(double anglepoint,double radiuspoint, double anglestart, double angleend,double radiuscircle)
        {
            bool b1 = AreClockwise(angleend, anglepoint);
            if (!b1)
                return false;

            bool b2 = AreClockwise(anglepoint, anglestart);
            if (!b2)
                return false;

            bool b3 = IsWithinRadius(radiuspoint, radiuscircle);
            if (!b3)
                return false;

            return true;
        }
    }
}
