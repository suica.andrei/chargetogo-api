﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class PolarToCartesian
    {
        public double X { get; set; }

        public double Y { get; set; }

        public PolarToCartesian (double Radius,double AngleDegree)
        {
            double AngleRadian = AngleDegree * 2 * Math.PI / 360;

            this.X = Radius * Math.Cos(AngleRadian);

            this.Y = Radius * Math.Sin(AngleRadian);
        }
    }
}
