﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class Route
    {
        public List<ChargePointDto> ListChargePoints { get; set; }

        public string Polyline { get; set; }

        public Route(List<ChargePointDto> listcharge,string polyline)
        {
            ListChargePoints = listcharge;

            Polyline = polyline;
        }
    }
}
