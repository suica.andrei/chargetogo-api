﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class Step
    {
        public Coordinate StartLocation { get; set; }

        public Coordinate EndLocation { get; set; }

        public double Distance { get; set; }

        public string Units { get; set; }

        public Step(Coordinate start, Coordinate end, double distance,string units)
        {
            StartLocation = start;

            EndLocation = end;

            Distance = distance;

            Units = units;
        }
    }
}
