﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChargeToGo.API.Models
{
    public class UserDto
    {
        [Required]
        [MaxLength(12)]
        public string Username { get; set; }

        [Required]
        [MaxLength(12)]
        public string Password { get; set; }
    }
}
